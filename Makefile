maven-install:
	mvn clean install

docker-compose-up:
	docker-compose up -d --build

docker-compose-down:
	docker-compose down -v