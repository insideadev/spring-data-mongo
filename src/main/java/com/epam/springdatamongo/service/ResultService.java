package com.epam.springdatamongo.service;

import com.epam.springdatamongo.entity.Result;
import com.epam.springdatamongo.entity.ResultStatus;

import java.util.List;

public interface ResultService {

    Result save(Result result);

    void updateByStatus(ResultStatus status);

    List<Result> findAll();
}
