package com.epam.springdatamongo.service.kafka;

public interface KafkaService {
    void sendMessage(String message, long times) throws InterruptedException;
}
