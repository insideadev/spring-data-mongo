package com.epam.springdatamongo.service.kafka;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.util.function.Consumer;

import static com.epam.springdatamongo.entity.ResultStatus.FAILED;
import static com.epam.springdatamongo.entity.ResultStatus.SUCCESS;

@Component
@Slf4j
public class KafkaProducer {

    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;

    public Void send(String topic, String key, Object value, Consumer<String> consumer) {
        try {
            kafkaTemplate.send(topic, key, value)
                    .completable()
                    .whenComplete((result, t) -> {
                        System.out.println();
                        if (result != null) {
                            var metadata = result.getRecordMetadata();
                            log.info("Thread {} - Successfully sent to topic: {}, partition:{}, offset:{}, message: {}", Thread.currentThread().getName(),
                                    topic, metadata.partition(), metadata.offset(), value);

                            if (consumer != null) {
                                consumer.accept(SUCCESS.name());
                            }
                        } else {
                            log.error("Thread {} - Error when sending message to topic: {} ", Thread.currentThread().getName(), topic, t);

                            if (consumer != null) {
                                consumer.accept(FAILED.name());
                            }
                        }
                    });
        } catch (Exception e) {
            log.error("Thread {} - An exception occurs when sending message to topic: {} ", Thread.currentThread().getName(), topic, e);

            if (consumer != null) {
                consumer.accept(FAILED.name());
            }
        }

        return null;
    }
}
