package com.epam.springdatamongo.service.kafka.impl;

import com.epam.springdatamongo.service.ResultService;
import com.epam.springdatamongo.service.kafka.KafkaProducer;
import com.epam.springdatamongo.service.kafka.KafkaService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.LongStream;

import static com.epam.springdatamongo.entity.ResultStatus.SUCCESS;

@Slf4j
@Service
@RequiredArgsConstructor
public class KafkaServiceImpl implements KafkaService {

    private final KafkaProducer kafkaProducer;

    private final ResultService resultService;

    @Override
    public void sendMessage(String message, long times) {
        ExecutorService es = Executors.newFixedThreadPool(100);

//        Callable<Void> callable = () -> kafkaProducer.send(HELLO_TOPIC, message, message, resultService::updateByStatus);

        LongStream.rangeClosed(1, times)
                .forEach(i -> {
                    es.submit(() -> resultService.updateByStatus(SUCCESS));
                });

    }
}
