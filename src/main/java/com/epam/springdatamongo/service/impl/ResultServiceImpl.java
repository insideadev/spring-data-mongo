package com.epam.springdatamongo.service.impl;

import com.epam.springdatamongo.entity.Result;
import com.epam.springdatamongo.entity.ResultStatus;
import com.epam.springdatamongo.repository.ResultRepository;
import com.epam.springdatamongo.service.ResultService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class ResultServiceImpl implements ResultService {

    private final ResultRepository repository;

    private final MongoTemplate mongoTemplate;

    @Override
    public Result save(Result result) {
        return repository.save(result);
    }

    @Override
    @Transactional
    public void updateByStatus(ResultStatus status) {
        Result result = repository.findByStatus(status.name()).orElseThrow(RuntimeException::new);
        Long counter = result.getCounter();
        result.setCounter(++counter);
    }

    @Override
    public List<Result> findAll() {
        return repository.findAll();
    }
}
