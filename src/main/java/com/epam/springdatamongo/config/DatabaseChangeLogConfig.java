package com.epam.springdatamongo.config;

import com.epam.springdatamongo.entity.Result;
import com.epam.springdatamongo.repository.ResultRepository;
import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;

import java.util.List;

import static com.epam.springdatamongo.entity.ResultStatus.FAILED;
import static com.epam.springdatamongo.entity.ResultStatus.SUCCESS;

@ChangeLog
public class DatabaseChangeLogConfig {

    @ChangeSet(order = "001", id = "seedDatabase", author = "Ken")
    public void seedDatabase(ResultRepository resultRepository) {
        List<Result> results = List.of(
                Result.builder().id("1")
                        .status(SUCCESS)
                        .counter(0L)
                        .build(),
                Result.builder().id("2")
                        .status(FAILED)
                        .counter(0L)
                        .build()
        );

        resultRepository.insert(results);
    }


}