package com.epam.springdatamongo.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

import static com.epam.springdatamongo.constant.KafkaConstant.*;

@Configuration
public class KafkaTopicConfig {

    @Bean
    public NewTopic orderTopic() {
        return TopicBuilder.name(HELLO_TOPIC)
                .partitions(PARTITIONS_NUMBER)
                .build();
    }

}
