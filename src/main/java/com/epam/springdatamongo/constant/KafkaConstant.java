package com.epam.springdatamongo.constant;

public class KafkaConstant {
    public static final String HELLO_TOPIC = "mongo.hello";
    public static final int PARTITIONS_NUMBER = 3;
}
