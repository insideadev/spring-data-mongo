package com.epam.springdatamongo.entity;

public enum ResultStatus {
    SUCCESS, FAILED
}
