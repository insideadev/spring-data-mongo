package com.epam.springdatamongo.entity;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Document
public class Result {
    @Id
    private String id;

    @Indexed(unique = true)
    private ResultStatus status;

    private Long counter;
}
