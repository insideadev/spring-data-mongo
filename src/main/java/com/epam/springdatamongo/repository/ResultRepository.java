package com.epam.springdatamongo.repository;

import com.epam.springdatamongo.entity.Result;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ResultRepository extends MongoRepository<Result, String> {

    @Query("{'status': ?0}")
    Optional<Result> findByStatus(String status);
}
