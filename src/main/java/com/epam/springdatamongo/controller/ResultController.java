package com.epam.springdatamongo.controller;

import com.epam.springdatamongo.entity.Result;
import com.epam.springdatamongo.service.ResultService;
import com.epam.springdatamongo.service.kafka.KafkaService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/results")
@RequiredArgsConstructor
public class ResultController {

    private final ResultService resultService;
    private final KafkaService kafkaService;

    @PostMapping
    public ResponseEntity<?> createResult(@RequestBody Result result) {
        return ResponseEntity.ok(resultService.save(result));
    }

    @PostMapping("/message")
    public ResponseEntity<?> sendMessage(@RequestHeader String message,
                                         @RequestHeader long times) throws InterruptedException {
        kafkaService.sendMessage(message, times);
        return ResponseEntity.ok(null);
    }

    @GetMapping
    public ResponseEntity<?> findAll() {
        return ResponseEntity.ok(resultService.findAll());
    }

}
